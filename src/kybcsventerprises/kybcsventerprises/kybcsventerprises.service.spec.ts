import { Test, TestingModule } from '@nestjs/testing';
import { KybcsventerprisesService } from './kybcsventerprises.service';

describe('KybcsventerprisesService', () => {
  let service: KybcsventerprisesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [KybcsventerprisesService],
    }).compile();

    service = module.get<KybcsventerprisesService>(KybcsventerprisesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
