import { Test, TestingModule } from '@nestjs/testing';
import { TiimeController } from './tiime.controller';

describe('Tiime Controller', () => {
  let controller: TiimeController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [TiimeController],
    }).compile();

    controller = module.get<TiimeController>(TiimeController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
