import { Module } from '@nestjs/common';
import { KybcsvactionnairesService } from './kybcsvactionnaires/kybcsvactionnaires.service';
import { KybcsvactionnairesController } from './kybcsvactionnaires/kybcsvactionnaires.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Kybcsvactionnaire } from './kybcsvactionnaire.entity';
import { MulterModule } from '@nestjs/platform-express';


@Module({
  imports: [
    TypeOrmModule.forFeature([Kybcsvactionnaire]),
    MulterModule.register({
      dest: './upload/csv',
    }),
  ],
  providers: [KybcsvactionnairesService],
  controllers: [KybcsvactionnairesController],
  exports: [KybcsvactionnairesService]
})
export class KybcsvactionnairesModule { }
