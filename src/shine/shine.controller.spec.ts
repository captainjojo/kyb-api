import { Test, TestingModule } from '@nestjs/testing';
import { ShineController } from './shine.controller';

describe('Shine Controller', () => {
  let controller: ShineController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ShineController],
    }).compile();

    controller = module.get<ShineController>(ShineController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
