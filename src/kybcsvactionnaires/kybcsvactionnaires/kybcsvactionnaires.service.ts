import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Kybcsvactionnaire } from '../kybcsvactionnaire.entity';
import { UpdateResult, DeleteResult } from  'typeorm';
import csv = require('fast-csv');
//import { createReadStream, unlinkSync } from 'fs';

@Injectable()
export class KybcsvactionnairesService {
    constructor(
        @InjectRepository(Kybcsvactionnaire)
        private kybcsvactionnaireRepository: Repository<Kybcsvactionnaire>,
    ) { }

    // List all shareholder Files imported
    async findAllImportedFiles(): Promise<Kybcsvactionnaire[]> {        
        return await this.kybcsvactionnaireRepository.createQueryBuilder("Kybcsvactionnaire")
            .select('DISTINCT importfile')
            .getRawMany();
    }

    // List all shareholder linked to one project
    async findAllShareholders(id: String): Promise<Kybcsvactionnaire[]> {
        //console.log(id);
        return await this.kybcsvactionnaireRepository.find({ where: { projectId: id } });
    }

    // Display data from one customer file
    async findOne(glname : String): Promise<Kybcsvactionnaire[]> {
        //console.log(glname);
        return await this.kybcsvactionnaireRepository.find({ where: { importfile: glname } });
    }

    // Import CSV File in DB
    async importCsvData(csvData: any[]): Promise<void> {
        console.log(csvData.length + ' lines to import');

         await this.kybcsvactionnaireRepository.createQueryBuilder("Kybcsvactionnaire")
        .insert()
        .into(Kybcsvactionnaire)
        .values(csvData)
        .execute();
        
        return ;
    }

    async create(kybcsvactionnaire: Kybcsvactionnaire): Promise<Kybcsvactionnaire> {
        return await this.kybcsvactionnaireRepository.save(kybcsvactionnaire);
    }

    async update(kybcsvactionnaire: Kybcsvactionnaire): Promise<UpdateResult> {
        return await this.kybcsvactionnaireRepository.update(kybcsvactionnaire.id, kybcsvactionnaire);
    }

    async delete(id): Promise<DeleteResult> {
        return await this.kybcsvactionnaireRepository.delete(id);
    }


}
