import { Test, TestingModule } from '@nestjs/testing';
import { KybcsvactionnairesService } from './kybcsvactionnaires.service';

describe('KybcsvactionnairesService', () => {
  let service: KybcsvactionnairesService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [KybcsvactionnairesService],
    }).compile();

    service = module.get<KybcsvactionnairesService>(KybcsvactionnairesService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
