import { Module } from '@nestjs/common';
import { KybdocsService } from './kybdocs/kybdocs.service';
import { KybdocsController } from './kybdocs/kybdocs.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Kybdoc } from './kybdoc.entity';
import { MulterModule } from '@nestjs/platform-express';


@Module({
  imports: [
    TypeOrmModule.forFeature([Kybdoc]),
    MulterModule.register({
      dest: './upload',
    }),
  ],
  providers: [KybdocsService],
  controllers: [KybdocsController]
})
export class KybdocsModule { }
