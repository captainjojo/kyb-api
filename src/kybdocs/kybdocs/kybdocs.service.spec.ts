import { Test, TestingModule } from '@nestjs/testing';
import { KybdocsService } from './kybdocs.service';

describe('KybdocsService', () => {
  let service: KybdocsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [KybdocsService],
    }).compile();

    service = module.get<KybdocsService>(KybdocsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
