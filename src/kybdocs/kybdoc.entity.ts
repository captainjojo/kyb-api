import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Kybdoc {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    path: string;

    @Column({ nullable: true })
    status: string;

    @Column({ nullable: true })
    type: string;
}