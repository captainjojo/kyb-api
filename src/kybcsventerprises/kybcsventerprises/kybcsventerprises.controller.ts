import { UseGuards, Controller, Get, Post, Put, Body, Param, UploadedFiles, UseInterceptors, Query } from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { Kybcsventerprise } from '../kybcsventerprise.entity';
import { KybcsventerprisesService } from './kybcsventerprises.service';
import * as fs from 'fs';
import * as csv from 'fast-csv';
//import { file } from '@babel/types';
import { AuthGuard } from '@nestjs/passport';


@Controller('Kybcsventerprises')
export class KybcsventerprisesController {
    constructor(private kybcsventerprisesService: KybcsventerprisesService) { }

    // List all Enterprises Projects
    @UseGuards(AuthGuard('jwt'))
    @Get()
    index(): Promise<Kybcsventerprise[]> {
        return this.kybcsventerprisesService.findAllProjects();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get('/search')
    search(@Query() query): Promise<Kybcsventerprise[]> {
        return this.kybcsventerprisesService.findAllProjectsByName(query.projectName);
    }

    // List all imported project files
    @Get('importedcsvfiles')
    async searchShareholders(): Promise<Kybcsventerprise[]> {
        return this.kybcsventerprisesService.findAllImportedFile();
    }

    // Get one Project data
    @UseGuards(AuthGuard('jwt'))
    @Get(':id/search')
    async displayOne(@Param('id') id: string): Promise<Kybcsventerprise[]> {
        return this.kybcsventerprisesService.findOne(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('create')
    async create(@Body() kybcsventerpriseData: Kybcsventerprise): Promise<any> {
        return this.kybcsventerprisesService.create(kybcsventerpriseData);
    }

    @UseGuards(AuthGuard('jwt'))
    @Put('update')
    async update(@Body() kybcsventerpriseData: Kybcsventerprise): Promise<any> {
        return this.kybcsventerprisesService.update(kybcsventerpriseData);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('upload')
    @UseInterceptors(AnyFilesInterceptor())
    async uploadedFile(@UploadedFiles() files) {
        console.log(files);
        const response = {
            path: files[0].path,
            filename: files[0].filename,
        };

        // Start to Import file in DB
        console.log('Start to Read Data from Csv file.');
        let stream = fs.createReadStream(files[0].path);
        let glfilename = 'gl' + files[0].filename + Date.now();
        let csvData = []; // Used for line reformating
        let glservice = this.kybcsventerprisesService;
        console.log('degdfsgdsg');
        let csvStream = csv
            .parse({ headers: true, delimiter: ';' })
            .on("data", (data) => {
                // Parse csv file to a Json String line by line                
                console.log({ ...data, importfile: glfilename });
                csvData.push({ ...data, importfile: glfilename });
                //console.log(csvData);                
            })
            .on("end", function () {
                //Import Data in DB        

                glservice.importCsvData(csvData);

            });

        stream.pipe(csvStream);

        return response;
    }



}
