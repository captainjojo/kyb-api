import { Module } from '@nestjs/common';
import { ShineController } from './shine.controller';
import { MulterModule } from '@nestjs/platform-express';
import { KybcsvactionnairesModule } from '../kybcsvactionnaires/kybcsvactionnaires.module';
import { KybcsventerprisesModule } from '../kybcsventerprises/kybcsventerprises.module';

@Module({
    imports: [
        KybcsvactionnairesModule,
        KybcsventerprisesModule,
        MulterModule.register({
            dest: './upload',
        }),
    ],
    controllers: [ShineController]
})
export class ShineModule { }
