import { Entity, Column, PrimaryColumn, CreateDateColumn } from 'typeorm';

@Entity()
export class Kybcsventerprise {
    @PrimaryColumn()
    projectId: string;

    @Column()
    projectNom: string;

    @Column()
    enterpriseName: string;

    @Column()
    commercialName: string;

    @Column()
    enterpriseLegalForm: string;

    @Column()
    mainActivity: string;

    @Column()
    streetNumber: string;

    @Column()
    streetName: string;

    @Column()
    postCode: string;

    @Column()
    city: string;

    @Column()
    enterpriseCapital: number;

    @Column()
    cashContribution: number;

    @Column()
    otherContribution: number;

    @Column()
    nominalValue: number;

    @Column()
    shareNumber: number;

    @Column()
    importfile: string;

    @Column({ nullable: true })
    status: string;

    @CreateDateColumn()
    createdAt: string;

    @Column({ nullable: true })
    validatedKycAt: string;

    @Column({ nullable: true })
    giveMoney: string;

    @Column({ nullable: true })
    sourceMoney: string;

    @Column({ nullable: true })
    profilClient: string;

    @Column({ nullable: true })
    localisationClient: string;

    @Column({ nullable: true })
    revenu: number;

    @Column({ nullable: true })
    registerNumber: number;

    @Column({ nullable: true })
    registerCountry: string;

    @Column({ nullable: true })
    registerAuthority: string;

    @Column({ nullable: true })
    registerDate: string;

    @Column({ nullable: true })
    registerLink: string;

    @Column({ nullable: true })
    statusFATCA: string;

    @Column({ nullable: true })
    typeFATCA: string;

    @Column({ nullable: true })
    GIIN: string;

    @Column({ nullable: true })
    statusCRS: string;

    @Column({ nullable: true })
    typeCRS: string;

    @Column({ nullable: true })
    fiscalityIdentification: string;

    @Column({ nullable: true })
    riskLevel: string;

    @Column({ nullable: true })
    notesKBIS: string;

    @Column({ nullable: true })
    notesStatus: string;

    @Column({ nullable: true })
    notesAproval: string;

    @Column({ nullable: true })
    notesHome: string;

    @Column({ nullable: true })
    notesFinalcialState: string;

    @Column({ nullable: true })
    notesAutocertification: string;

    @Column({ nullable: true })
    notesFATCA: string;

    @Column({ nullable: true })
    accredidator: string;
}