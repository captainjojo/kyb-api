import { Controller, Post, UseGuards, UseInterceptors, UploadedFiles, Redirect } from '@nestjs/common';
import { KybcsvactionnairesService } from '../kybcsvactionnaires/kybcsvactionnaires/kybcsvactionnaires.service';
import { KybcsventerprisesService } from '../kybcsventerprises/kybcsventerprises/kybcsventerprises.service';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import * as fs from 'fs';
import { AuthGuard } from '@nestjs/passport';
import { Kybcsventerprise } from '../kybcsventerprises/kybcsventerprise.entity';
import { Kybcsvactionnaire } from '../kybcsvactionnaires/kybcsvactionnaire.entity';

@Controller('shine')
export class ShineController {
    constructor(
        private kybcsventerprisesService: KybcsventerprisesService,
        private kybcsvactionnairesService: KybcsvactionnairesService) { }

    @UseGuards(AuthGuard('jwt'))
    @Post('/upload')
    @UseInterceptors(AnyFilesInterceptor())
    async uploadedFile(@UploadedFiles() files) {
        let rawdata = fs.readFileSync(files[0].path);
        let data = JSON.parse(rawdata.toString());

        this.kybcsventerprisesService.create({
            projectId: data.company.legalName,
            projectNom: data.company.legalName,
            enterpriseName: data.company.legalName,
            commercialName: data.company.tradeName,
            enterpriseLegalForm: data.company.legalForm,
            mainActivity: data.company.activity,
            streetNumber: data.company.street,
            streetName: data.company.street,
            postCode: data.company.zip,
            city: data.company.city,
            enterpriseCapital: data.company.shareCapitalTotal,
            cashContribution: data.company.shareCapitalCash,
            otherContribution: 0,
            nominalValue: data.company.shareValue,
            shareNumber: data.company.numberOfShares,
            importfile: files[0].path
        } as Kybcsventerprise);

        Object.keys(data.shareholders).forEach((key) => {
            this.kybcsvactionnairesService.create({
                projectId: data.company.legalName,
                associateId: data.shareholders[key].lastName + data.shareholders[key].firstName,
                projectNom: data.company.legalName,
                mandateType: data.shareholders[key].gender,
                associateName: data.shareholders[key].lastName,
                associateFirstname: data.shareholders[key].firstName,
                streetNumberAndName: '',
                mainActivity: '',
                postCode: '',
                city: '',
                associateNationality: data.shareholders[key].nationality,
                associatePhonenumber: '',
                birthCity: data.shareholders[key].birthCountry,
                birthPostalcode: '',
                finantialContribution: data.shareholders[key].legalShares,
                residenceCountry: data.shareholders[key].country,
                birthCountry: '',
                importfile: files[0].path
            } as Kybcsvactionnaire);

        });
    }
}
