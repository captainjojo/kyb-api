import { Controller, Request, Post, UseGuards } from '@nestjs/common';
import { MailerService } from '@nest-modules/mailer';
import { AuthGuard } from '@nestjs/passport';
import { KybcsvactionnairesService } from '../kybcsvactionnaires/kybcsvactionnaires/kybcsvactionnaires.service';

@Controller('tiime')
export class TiimeController {
    constructor(
        private readonly kybcsvactionnairesService: KybcsvactionnairesService,
        private readonly mailerService: MailerService) { }

    @UseGuards(AuthGuard('jwt'))
    @Post('sendMail')
    async sendMail(@Request() req) {
        const actionnaires = await this.kybcsvactionnairesService.findAllShareholders(req.body.projectId);
        actionnaires.forEach((actionnaire) => {
            if (actionnaire.email && actionnaire.associateFirstname) {
                this
                    .mailerService
                    .sendMail({
                        to: actionnaire.email,
                        from: 'tiime@dittobank.com',
                        subject: 'Objet : Déposez le capital social de votre société',
                        template: 'iban', // The `.pug` or `.hbs` extension is appended automatically.
                        context: {  // Data to be sent to template engine.
                            firstname: actionnaire.associateFirstname,
                            iban: req.body.iban,
                        },
                    })
                    .then((e) => { console.log(e) })
                    .catch((e) => { console.log(e) });
            }
        })
        /*
            
     */
        return { status: 'Success' }
    }

}
