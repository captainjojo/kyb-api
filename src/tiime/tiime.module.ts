import { Module } from '@nestjs/common';
import { TiimeController } from './tiime.controller';
import { KybcsvactionnairesModule } from '../kybcsvactionnaires/kybcsvactionnaires.module';
import { MailerModule, HandlebarsAdapter } from '@nest-modules/mailer';

@Module({
    imports: [
        KybcsvactionnairesModule,
        MailerModule.forRootAsync({
            useFactory: () => ({
                transport:
                {
                    pool: true,
                    host: "smtp.gmail.com",
                    port: 465,
                    secure: true, // use TLS
                    auth: {
                        user: "jonathan.jalouzot@gmail.com",
                        pass: "MON PASSWORD"
                    }
                },
                defaults: {
                    from: '"nest-modules" <modules@nestjs.com>',
                },
                template: {
                    dir: './templates',
                    adapter: new HandlebarsAdapter(), // or new PugAdapter()
                    options: {
                        strict: true,
                    },
                },
            }),
        })
    ],
    controllers: [TiimeController]
})
export class TiimeModule { }
