import { UseGuards, Controller, Get, Post, Put, Body, Param, Res, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { KybdocsService } from './kybdocs.service';
import * as fs from 'fs';
//import { file } from '@babel/types';
import { Kybdoc } from '../kybdoc.entity';
import { AuthGuard } from '@nestjs/passport';


@Controller('kybdocs')
export class KybdocsController {
    constructor(private kybdocsService: KybdocsService) {
        fs.existsSync('./upload/zip/') || fs.mkdirSync('./upload/zip/');
        fs.existsSync('./upload/kybdocs/') || fs.mkdirSync('./upload/kybdocs/');
    }

    // List all Kybdoc
    //@Get()
    //index(): Promise<Kybdoc[]> {        
    //    return ;
    //}

    // Display one specific kyb document
    @UseGuards(AuthGuard('jwt'))
    @Get(':projectId/:fileId/display')
    async serveKybdoc(@Param('projectId') projectId: string, @Param('fileId') fileId: string, @Res() res): Promise<any> {

        projectId = projectId.replace('..', '');
        projectId = projectId.replace('//', '');
        projectId = projectId.replace('/', '');
        projectId = projectId.replace('.', '');
        fileId = fileId.replace('..', '');
        fileId = fileId.replace('//', '');
        fileId = fileId.replace('/', '');
        //console.log(projectId+'   '+fileId);
        res.sendFile(fileId, { root: 'upload/kybdocs/' + projectId + '/' });
    }

    // List Project files
    @UseGuards(AuthGuard('jwt'))
    @Get(':projectid/search')
    async listFiles(@Param('projectid') id: string): Promise<any[]> {
        let response = [];
        //console.log(id);
        // Secure Path moves
        //id = '../Archive';
        id = id.replace('..', '');
        id = id.replace('//', '');
        id = id.replace('/', '');
        id = id.replace('.', '');
        let projectDir = './upload/kybdocs/' + id;
        let uploadDir = 'upload/kybdocs/' + id;
        //console.log(projectDir);

        let files = fs.readdirSync(projectDir);

        await Promise.all(files.map(async file => {
            let tmp;
            tmp = JSON.parse(JSON.stringify(file));
            let tmpPath = uploadDir + '/' + tmp;

            const kybdoc = await this.kybdocsService.get(tmpPath);
            response.push({ filename: tmp, status: kybdoc && kybdoc.status, type: kybdoc && kybdoc.type, ext: tmp.split('.').pop() });
        }));

        return response;
    }

    // Upload zipped KYB Docs
    @UseGuards(AuthGuard('jwt'))
    @Post('upload')
    @UseInterceptors(AnyFilesInterceptor())
    async uploadedFile(@UploadedFiles() files) {
        // Start to Import file in DB
        console.log('New Zip file imported.');
        const response = {
            path: files[0].destination + '/zip/' + files[0].originalname,
            filename: files[0].originalname,
        };

        if (files[0].mimetype != 'application/zip') {
            fs.unlink(files[0].path, (err) => {
                if (err) throw err;
            })
        } else {
            fs.rename(files[0].path, files[0].destination + '/zip/' + files[0].originalname, function (err) {
                if (err) {
                    throw err;
                    console.log(err);
                }
                else {
                    console.log('File renamed');

                }
            });
        }
        return response;
    }

    // Unzip KYB Docs
    @UseGuards(AuthGuard('jwt'))
    @Post('unzip')
    async unzipKYBFile(@Body() sourcePath: string): Promise<any> {
        var extract = require('extract-zip');

        let zipname = JSON.parse(JSON.stringify(sourcePath));

        const unpackPath = 'upload/kybdocs/';
        const resolve = require('path').resolve;
        var resolvedUnpackPath = resolve(unpackPath);
        resolvedUnpackPath = resolvedUnpackPath + '/' + zipname.source.replace('./upload/zip/', '');
        resolvedUnpackPath = resolvedUnpackPath.replace('.zip', '');

        //console.log(sourcePath); 
        //console.log(zipname.source);         
        console.log(resolvedUnpackPath);

        //Extract File
        extract(zipname.source, { dir: resolvedUnpackPath }, (err) => {
            if (err) {
                console.log(err);
            }
            else {
                fs.unlink(zipname.source, (err) => { });
                console.log('Files Extdracted & deleted !!!');

                let files = fs.readdirSync(resolvedUnpackPath);
                const pathArray = resolvedUnpackPath.split('/');
                files.forEach(file => {
                    let tmpPath = pathArray[pathArray.length - 3] + '/' + pathArray[pathArray.length - 2] + '/' + pathArray[pathArray.length - 1] + '/' + file;
                    this.kybdocsService.create({ path: tmpPath, status: null } as Kybdoc);

                });
            }
        });

        return { status: 'Success' };

    }

    @UseGuards(AuthGuard('jwt'))
    @Put('update')
    async create(@Body() kybdoc: Kybdoc): Promise<any> {
        return this.kybdocsService.update(kybdoc);
    }
}

