import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { KybdocsModule } from './kybdocs/kybdocs.module';
import { KybcsventerprisesModule } from './kybcsventerprises/kybcsventerprises.module';
import { KybcsvactionnairesModule } from './kybcsvactionnaires/kybcsvactionnaires.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { ShineModule } from './shine/shine.module';
import { TiimeModule } from './tiime/tiime.module';


@Module({
  //Port Mysql 3306
  imports: [KybdocsModule, KybcsventerprisesModule, KybcsvactionnairesModule, TypeOrmModule.forRoot({
    type: 'mysql',
    host: "localhost",
    username: "root",
    password: "password",
    database: "kyb",
    port: 3306,
    entities: [__dirname + '/**/*.entity{.ts,.js}'],
    synchronize: true,
  }), AuthModule, UsersModule, ShineModule, TiimeModule,],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {

}
