import { Test, TestingModule } from '@nestjs/testing';
import { KybcsvactionnairesController } from './kybcsvactionnaires.controller';

describe('Kybcsvactionnaire Controller', () => {
  let controller: KybcsvactionnairesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KybcsvactionnairesController],
    }).compile();

    controller = module.get<KybcsvactionnairesController>(KybcsvactionnairesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
