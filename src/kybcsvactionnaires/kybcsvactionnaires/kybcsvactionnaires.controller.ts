import { UseGuards, Controller, Get, Post, Put, Body, Param, UploadedFiles, UseInterceptors } from '@nestjs/common';
import { AnyFilesInterceptor } from '@nestjs/platform-express';
import { Kybcsvactionnaire } from '../kybcsvactionnaire.entity';
import { KybcsvactionnairesService } from './kybcsvactionnaires.service';
import * as fs from 'fs';
import * as csv from 'fast-csv';
import { AuthGuard } from '@nestjs/passport';


@Controller('kybcsvactionnaires')
export class KybcsvactionnairesController {
    constructor(private kybcsvactionnairesService: KybcsvactionnairesService) { }

    // List all shareholders in DB
    @UseGuards(AuthGuard('jwt'))
    @Get()
    index(): Promise<Kybcsvactionnaire[]> {
        return this.kybcsvactionnairesService.findAllImportedFiles();
    }

    @UseGuards(AuthGuard('jwt'))
    @Get(':idproject/projectshareholders')
    async searchShareholders(@Param('idproject') id: string): Promise<Kybcsvactionnaire[]> {
        //console.log(id);
        return this.kybcsvactionnairesService.findAllShareholders(id);
    }

    // Get one shareholder's data
    @UseGuards(AuthGuard('jwt'))
    @Get(':id/search')
    async glfind(@Param('id') id: string): Promise<Kybcsvactionnaire[]> {
        return this.kybcsvactionnairesService.findOne(id);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('create')
    async create(@Body() kybcsvactionnaireData: Kybcsvactionnaire): Promise<any> {
        return this.kybcsvactionnairesService.create(kybcsvactionnaireData);
    }

    @UseGuards(AuthGuard('jwt'))
    @Put('update')
    async update(@Body() kybcsvactionnaireData: Kybcsvactionnaire): Promise<any> {
        return this.kybcsvactionnairesService.update(kybcsvactionnaireData);
    }

    @UseGuards(AuthGuard('jwt'))
    @Post('upload')
    @UseInterceptors(AnyFilesInterceptor())
    async uploadedFile(@UploadedFiles() files) {
        //console.log(files);      
        const response = {
            path: files[0].path,
            filename: files[0].filename,
        };

        // Start to Import file in DB
        console.log('Start to Read Data from Csv file.');
        let stream = fs.createReadStream(files[0].path);
        let glfilename = 'gl' + files[0].filename + Date.now();
        let csvData = []; // Used for line reformating
        let glservice = this.kybcsvactionnairesService;

        let csvStream = csv
            .parse({ headers: true, delimiter: ';' })
            .on("data", (data) => {
                // Parse csv file to a Json String line by line
                //console.log({...data, importfile: glfilename });
                csvData.push({ ...data, importfile: glfilename });
                //console.log(csvData);                
            })
            .on("end", function () {
                //Import Data in DB                
                glservice.importCsvData(csvData);

            });

        stream.pipe(csvStream);

        return response;
    }



}
