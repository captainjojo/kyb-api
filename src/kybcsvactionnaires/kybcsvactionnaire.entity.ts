import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Kybcsvactionnaire {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    projectId: string;

    @Column()
    associateId: string;

    @Column()
    projectNom: string;

    @Column()
    mandateType: string;

    @Column()
    associateName: string;

    @Column()
    associateFirstname: string;

    @Column()
    mainActivity: string;

    @Column()
    streetNumberAndName: string;

    @Column()
    postCode: string;

    @Column()
    city: string;

    @Column()
    associateNationality: string;

    @Column()
    associatePhonenumber: string;

    @Column()
    birthCity: string;

    @Column()
    birthPostalcode: string;

    @Column()
    finantialContribution: number;

    @Column()
    residenceCountry: string;

    @Column()
    birthCountry: string;

    @Column()
    importfile: string;

    @Column({ nullable: true })
    birthday: string

    @Column({ nullable: true })
    screeningAML: string

    @Column({ nullable: true })
    actionnarialStructure: string

    @Column({ nullable: true })
    oparationNotice: string

    @Column({ nullable: true })
    noteIdentity: string

    @Column({ nullable: true })
    notesHome: string

    @Column({ nullable: true })
    notesNomination: string

    @Column({ nullable: true })
    notesProcuration: string

    @Column({ nullable: true })
    notesPublication: string

    @Column({ nullable: true })
    notesIBAN: string

    @Column({ nullable: true })
    email: string
}