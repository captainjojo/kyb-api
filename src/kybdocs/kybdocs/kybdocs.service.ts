import { Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Kybdoc } from '../kybdoc.entity';
import { UpdateResult, DeleteResult } from 'typeorm';
//import { createReadStream, unlinkSync } from 'fs';

@Injectable()
export class KybdocsService {
    constructor(
        @InjectRepository(Kybdoc)
        private kybdocRepository: Repository<Kybdoc>,
    ) { }

    async create(kybdoc: Kybdoc): Promise<Kybdoc> {
        return await this.kybdocRepository.save(kybdoc);
    }

    async update(kybdoc: Kybdoc): Promise<UpdateResult> {
        return await this.kybdocRepository.update({ path: kybdoc.path }, kybdoc);
    }

    async get(path: string): Promise<Kybdoc> {
        return await this.kybdocRepository.findOne({ where: { path: path } });
    }

}
