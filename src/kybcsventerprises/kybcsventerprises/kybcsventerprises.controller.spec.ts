import { Test, TestingModule } from '@nestjs/testing';
import { KybcsventerprisesController } from './kybcsventerprises.controller';

describe('Kybcsventerprise Controller', () => {
  let controller: KybcsventerprisesController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KybcsventerprisesController],
    }).compile();

    controller = module.get<KybcsventerprisesController>(KybcsventerprisesController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
