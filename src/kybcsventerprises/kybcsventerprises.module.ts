import { Module } from '@nestjs/common';
import { KybcsventerprisesService } from './kybcsventerprises/kybcsventerprises.service';
import { KybcsventerprisesController } from './kybcsventerprises/kybcsventerprises.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Kybcsventerprise } from './kybcsventerprise.entity';
import { MulterModule } from '@nestjs/platform-express';


@Module({
  imports: [
    TypeOrmModule.forFeature([Kybcsventerprise]),
    MulterModule.register({
      dest: './upload/csv',
    }),
  ],
  providers: [KybcsventerprisesService],
  controllers: [KybcsventerprisesController],
  exports: [KybcsventerprisesService]
})
export class KybcsventerprisesModule { }
