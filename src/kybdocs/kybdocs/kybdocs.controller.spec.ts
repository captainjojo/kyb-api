import { Test, TestingModule } from '@nestjs/testing';
import { KybdocsController } from './kybdocs.controller';

describe('Kybdoc Controller', () => {
  let controller: KybdocsController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [KybdocsController],
    }).compile();

    controller = module.get<KybdocsController>(KybdocsController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
