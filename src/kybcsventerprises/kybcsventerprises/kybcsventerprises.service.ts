import { Injectable } from '@nestjs/common';
import { Repository, Like } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { Kybcsventerprise } from '../kybcsventerprise.entity';
import { UpdateResult, DeleteResult } from 'typeorm';
import csv = require('fast-csv');
//import { createReadStream, unlinkSync } from 'fs';

@Injectable()
export class KybcsventerprisesService {
    constructor(
        @InjectRepository(Kybcsventerprise)
        private kybcsventerpriseRepository: Repository<Kybcsventerprise>,
    ) { }

    // List all customer Files imported
    async findAllImportedFile(): Promise<Kybcsventerprise[]> {
        return await this.kybcsventerpriseRepository.createQueryBuilder("Kybcsventerprise")
            .select('DISTINCT importfile')
            .getRawMany();
    }

    // List all imported Enterprises projects
    async findAllProjects(): Promise<Kybcsventerprise[]> {
        return await this.kybcsventerpriseRepository.find();
    }

    // List all imported Enterprises projects
    async findAllProjectsByName(projectNom: String): Promise<Kybcsventerprise[]> {
        return await this.kybcsventerpriseRepository.createQueryBuilder('Kybcsventerprise')
            .where("projectNom like  :projectNom", { projectNom: '%' + projectNom + '%' })
            .orWhere("status like  :projectNom", { projectNom: '%' + projectNom + '%' })
            .getMany();


    }

    // Display data from one project 
    async findOne(idproject: String): Promise<Kybcsventerprise[]> {
        return await this.kybcsventerpriseRepository.find({ where: { projectId: idproject } });
    }

    // Import CSV File in DB
    async importCsvData(csvData: any[]): Promise<void> {
        console.log(csvData.length + ' lines to imffffport');
        console.log(csvData);
        await this.kybcsventerpriseRepository.createQueryBuilder("Kybcsventerprise")
            .insert()
            .into(Kybcsventerprise)
            .values(csvData)
            .execute();

        return;
    }

    async create(kybcsventerprise: Kybcsventerprise): Promise<Kybcsventerprise> {
        return await this.kybcsventerpriseRepository.save(kybcsventerprise);
    }

    async update(kybcsventerprise: Kybcsventerprise): Promise<UpdateResult> {
        return await this.kybcsventerpriseRepository.update(kybcsventerprise.projectId, kybcsventerprise);
    }

    async delete(id): Promise<DeleteResult> {
        return await this.kybcsventerpriseRepository.delete(id);
    }


}
